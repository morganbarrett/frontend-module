<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Business Fox</title>

        <link href="../core/includes/css/bootstrap.css" rel="stylesheet">

        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <link href="../modules/frontend/includes/css/creative.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="page-top">
		<include src="common.tpl"></include>

		<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		    <div class="container-fluid">
		        <div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
		            </button>
		            <a class="navbar-brand page-scroll" href="#page-top">
						<img src="../bts/includes/img/logo.png" height="24" style="display:inline-block"> <span>Business Fox</span>
					</a>
		        </div>

		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		            <ul class="nav navbar-nav navbar-right">
		                <foreach values="cms.sections">
		                    <wrap>li<if true="key == currentPage"> class="active"</if></wrap>
								<function call="changePage">
									<var set="section">frontend</var>
									<var set="page"><var get="key"></var></var>
									<var set="label">
										<function call="icon"><var get="value.icon"></var></function> <var get="value.label"></var>
									</var>
		                        </function>
		                    </li>
		                </foreach>
						<li>
							<function call="changePage">
								<var set="section">login</var>
								<var set="page">index</var>
								<var set="label">
									<function call="icon">lock</function> Login
								</var>
							</function>
						</li>
		            </ul>
		        </div>
		    </div>
		</nav>

        <include>body</include>

		<div id="messagesContainer">
			<div class="tooltip">
				Give us a message now to get started!
			</div>
			<div class="messageIcon"></div>
			<div class="messageBadge">
				<function call="icon">comment</function>
			</div>
		</div>

		<script src="../core/includes/js/jquery.js"></script>
		<script src="../core/includes/js/bootstrap.js"></script>

		<!-- Plugin JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

		<script src="../modules/frontend/includes/js/creative.js"></script>
    </body>
</html>
