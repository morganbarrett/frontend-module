<function set="outputButton">
    <wrap>a href="#about" class="btn btn-<var get="colour"></var> btn-xl page-scroll"</wrap>
        <var get="label"></var>
    </a>
</function>

<function set="outputHeader">
    <wrap>header class="header <var get="class"></var>"</wrap>
        <div class="header-content">
            <div class="header-content-inner">
                <img src="../bts/includes/img/logo.png" height="200">
                <h1 id="homeHeading">
                    <var get="label"></var>
                </h1>
                <hr>
                <var get="description"></var>
            </div>
        </div>
    </header>
</function>

<function set="outputSection">
    <wrap>section class="bg-<var get="colour"></var> <var get="class"></var>" id="<var get="id"></var>"</wrap>
        <if true="noLabel != 'true'">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="section-heading">
                            <var get="label"></label>
                        </h2>
                        <wrap>hr class="
                            <if true="colour == 'primary'">light</if>
                            <if true="colour == 'light'">primary</if>
                        "</wrap>
                        <var get="description"></var>
                    </div>
                </div>
            </div>
        </if>
        <var get="body"></var>
    </section>
</function>

<function set="outputRow">
    <div class="container">
        <div class="row">
            <foreach values="items">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
                        <h3>
                            <var get="label"></var>
                        </h3>
                        <p class="text-muted">
                            <var get="description"></var>
                        </p>
                    </div>
                </div>
            </foreach>
        </div>
    </div>
</function>

<function set="outputGrid">
    <div class="container-fluid">
        <div class="row no-gutter popup-gallery">
            <foreach values="items">
                <div class="col-lg-4 col-sm-6">
                    <div class="portfolio-box">
                        <img src="../modules/frontend/includes/img/portfolio/thumbnails/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    <var get="label"></var>
                                </div>
                                <div class="project-name">
                                    <var get="description"></var>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </foreach>
        </div>
    </div>
</function>


<!--<aside class="bg-dark">
    <div class="container text-center">
        <div class="call-to-action">
            <h2>Try our live demo</h2>
            <a href="http://startbootstrap.com/template-overviews/creative/" class="btn btn-default btn-xl sr-button">Download Now!</a>
        </div>
    </div>
</aside>-->
